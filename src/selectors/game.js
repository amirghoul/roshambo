import { propOr } from 'ramda';
import { INITIAL_SHAPES, LETS_BEGIN, HUMAN, COMPUTER, INITIAL_SCORE_STATISTIC } from '../game';

const human = { name: HUMAN };
const computer = { name: COMPUTER, isComputer: true };

export const getHuman = propOr(human, 'human');
export const getComputer = propOr(computer, 'computer');
export const getStatus = propOr(LETS_BEGIN, 'status');
export const getShapes = propOr(INITIAL_SHAPES, 'shapes');
export const getScoreHistory = propOr([], 'scoreHistory');
export const getScoreStatistics = propOr(INITIAL_SCORE_STATISTIC, 'scoreStatistics');

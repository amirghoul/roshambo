import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import theme from '../../theme';
import configureStore from '../../store';
import { getHuman, getComputer, getStatus, getShapes, getScoreHistory, getScoreStatistics } from '../../selectors/game';

const initialState = {
  human: getHuman(''),
  computer: getComputer(''),
  status: getStatus(''),
  shapes: getShapes(''),
  scoreHistory: getScoreHistory(''),
  scoreStatistics: getScoreStatistics(''),
};
const myStore = configureStore(initialState);

const AllProviders = ({ children }) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Provider store={myStore}>{children}</Provider>
  </ThemeProvider>
);

AllProviders.propTypes = {
  children: PropTypes.element,
};

export const customRender = (ui, options) => render(ui, { wrapper: AllProviders, options });
export * from '@testing-library/react';
export { customRender as render };

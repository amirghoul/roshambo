import React from 'react';
import { Grid, IconButton, Icon, makeStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { SHAPES, isPlaying, isWaitingResult } from '../../game';
import { getStatus } from '../../selectors/game';
import { startGame } from '../../actions/game';

const useStyles = makeStyles({
  innerItemCenter: {
    textAlign: 'center',
  },
});

const PlayerButtons = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const status = useSelector(state => getStatus(state));
  const playHandler = shape => dispatch(startGame(shape));

  return (
    <Grid container direction="row" justify="space-evenly" alignItems="center">
      <Grid item xs={4} className={classes.innerItemCenter}>
        <IconButton
          aria-label="delete"
          onClick={() => playHandler(SHAPES[0])}
          disabled={isPlaying(status) || isWaitingResult(status)}
        >
          <Icon className={SHAPES[0].icon} />
        </IconButton>
      </Grid>
      <Grid item xs={4} className={classes.innerItemCenter}>
        <IconButton
          aria-label="delete"
          onClick={() => playHandler(SHAPES[1])}
          disabled={isPlaying(status) || isWaitingResult(status)}
        >
          <Icon className={SHAPES[1].icon} />
        </IconButton>
      </Grid>
      <Grid item xs={4} className={classes.innerItemCenter}>
        <IconButton
          aria-label="delete"
          onClick={() => playHandler(SHAPES[2])}
          disabled={isPlaying(status) || isWaitingResult(status)}
        >
          <Icon className={SHAPES[2].icon} />
        </IconButton>
      </Grid>
    </Grid>
  );
};

PlayerButtons.propTypes = {};

export default PlayerButtons;

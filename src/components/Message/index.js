import React from 'react';
import propTypes from 'prop-types';
import { Typography, Grid, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  innerItemCenter: {
    textAlign: 'center',
  },
});

const Message = ({ status }) => {
  const classes = useStyles();
  return (
    <Grid className={classes.innerItemCenter}>
      <Typography variant="h2" component="h1" gutterBottom>
        {status}
      </Typography>
    </Grid>
  );
};

Message.propTypes = {
  status: propTypes.string,
};

export default Message;

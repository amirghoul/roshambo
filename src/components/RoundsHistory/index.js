import React from 'react';
import { Grid, List, ListItem, ListItemText, ListItemIcon, Icon, makeStyles, ListSubheader } from '@material-ui/core';
import { purple, blue, red } from '@material-ui/core/colors';
import { useSelector } from 'react-redux';
import { getScoreHistory } from '../../selectors/game';

const useStyles = makeStyles(theme => ({
  root: {
    minHeight: 344,
    maxHeight: 344,
  },
  rootList: {
    position: 'relative',
    overflow: 'auto',
    height: 344,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  innerItemCenter: {
    textAlign: 'center',
  },
  listItemIcon: {
    display: 'inline',
  },
}));

const RoundsHistory = () => {
  const classes = useStyles();
  const scoreHistory = useSelector(state => getScoreHistory(state));

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        <List
          dense
          subheader={<ListSubheader>Round {scoreHistory ? ` ${scoreHistory.length}` : '0'} </ListSubheader>}
          className={classes.rootList}
        >
          {scoreHistory
            .slice(scoreHistory.length > 10 ? scoreHistory.length - 10 : 0)
            .reverse()
            .map((itemValue, index) => {
              const { id, human, computer, winner } = itemValue;
              return (
                // eslint-disable-next-line react/no-array-index-key
                <ListItem data-testid="historyItem" key={index} button className={classes.innerItemCenter}>
                  <ListItemIcon
                    classes={{
                      root: classes.listItemIcon,
                    }}
                  >
                    <Icon
                      className={human.icon}
                      style={{ color: winner === null ? purple[200] : (winner.isComputer && red[500]) || blue[700] }}
                    />
                  </ListItemIcon>
                  <ListItemText primary={`Round ${id}`} />
                  <ListItemIcon
                    classes={{
                      root: classes.listItemIcon,
                    }}
                  >
                    <Icon
                      className={computer.icon}
                      style={{ color: winner === null ? purple[200] : (winner.isComputer && blue[500]) || red[700] }}
                    />
                  </ListItemIcon>
                </ListItem>
              );
            })}
        </List>
      </Grid>
    </Grid>
  );
};

export default RoundsHistory;

import React from 'react';
import { Grid, Card, CardActionArea, makeStyles } from '@material-ui/core';
import ScoreStatistic from '../ScoreStatistic';
import RoundsHistory from '../RoundsHistory';

const useStyles = makeStyles({
  root: {
    minHeight: 400,
    maxHeight: 400,
  },
});
const ScoreBoard = () => {
  const classes = useStyles();

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        <Card>
          <CardActionArea>
            <ScoreStatistic />
          </CardActionArea>
        </Card>
      </Grid>
      <Grid item xs={12}>
        <Card>
          <RoundsHistory />
        </Card>
      </Grid>
    </Grid>
  );
};

ScoreBoard.propTypes = {};

export default ScoreBoard;

import React from 'react';
import { render } from '../../../utils/testing';
import Component from '..';

const player = { name: 'amir' };

describe('components | HandDisplay | index', () => {
  it('should render component', () => {
    const { container } = render(<Component player={player} />);
    expect(container).toMatchSnapshot();
  });
});

import React from 'react';
import propTypes from 'prop-types';
import { Grid, Icon, makeStyles, CircularProgress } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { propOr } from 'ramda';
import { isPlaying, isComputer } from '../../game';
import { getStatus, getShapes } from '../../selectors/game';

const useStyles = makeStyles({
  innerItemCenter: {
    textAlign: 'center',
  },
});

const HandDisplay = ({ player }) => {
  const classes = useStyles();

  const status = useSelector(state => getStatus(state));
  const shapes = useSelector(state => getShapes(state));
  const shape = propOr('', player.name, shapes);

  return (
    <Grid className={classes.innerItemCenter}>
      {!(isPlaying(status) && isComputer(player)) ? (
        <Icon className={shape.icon} style={{ fontSize: 150 }} />
      ) : (
        <CircularProgress color="secondary" size={300} />
      )}
    </Grid>
  );
};

HandDisplay.propTypes = {
  player: propTypes.object,
};

export default HandDisplay;

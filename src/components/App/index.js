import React from 'react';
import { Grid, Paper, makeStyles } from '@material-ui/core';

import { useSelector } from 'react-redux';
import PlayerBoard from '../PlayerBoard';
import PlayerButtons from '../PlayerButtons';
import Message from '../Message';
import ScoreBoard from '../ScoreBoard';
import { getHuman, getComputer, getStatus } from '../../selectors/game';

const useStyles = makeStyles(() => ({
  marginTop: {
    marginTop: 20,
  },
  paperFullSize: {
    width: '100%',
    height: '100%',
  },
  innerItemCenter: {
    textAlign: 'center',
  },
}));

const App = () => {
  const classes = useStyles();
  const human = useSelector(state => getHuman(state));
  const computer = useSelector(state => getComputer(state));
  const status = useSelector(state => getStatus(state));

  return (
    <Grid container justify="center" direction="column" alignItems="center">
      <Grid
        item
        container
        xs={11}
        justify="space-evenly"
        direction="row"
        alignItems="stretch"
        spacing={2}
        className={classes.marginTop}
      >
        <Grid item xs={12} sm={6} md={4}>
          <PlayerBoard player={human}>
            <PlayerButtons />
            <Message status={status} />
          </PlayerBoard>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <Paper variant="outlined" className={classes.paperFullSize}>
            <ScoreBoard />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4}>
          <PlayerBoard player={computer} />
        </Grid>
      </Grid>
    </Grid>
  );
};

App.propTypes = {};

export default App;

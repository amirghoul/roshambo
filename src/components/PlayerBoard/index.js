import React from 'react';
import propTypes from 'prop-types';
import { makeStyles, Card, CardHeader, CardActionArea, CardContent, CardActions } from '@material-ui/core';
import HandDisplay from '../HandDisplay';

const useStyles = makeStyles({
  root: {
    minHeight: 400,
    position: 'relative',
  },
  cardHeader: {
    backgroundColor: '#e57373',
  },
  cardActionArea: {
    position: 'absolute',
    bottom: '0',
  },
});

const PlayerBoard = ({ children, player }) => {
  const classes = useStyles();

  const numberOfChildren = React.Children.count(children);
  return (
    <Card className={classes.root} elevation={6}>
      <CardHeader
        title={player.name}
        titleTypographyProps={{ align: 'center' }}
        subheaderTypographyProps={{ align: 'center' }}
        className={classes.cardHeader}
      />

      <CardActions>{numberOfChildren === 2 && children[0]}</CardActions>
      <CardActionArea className={classes.cardActionArea}>
        <CardContent>
          {numberOfChildren === 2 && children[1]}
          <HandDisplay player={player} />
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

PlayerBoard.propTypes = {
  player: propTypes.object.isRequired,
  children: propTypes.node,
};

export default PlayerBoard;

import React from 'react';
import { Grid, Typography, makeStyles } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { getScoreStatistics } from '../../selectors/game';

const useStyles = makeStyles({
  innerItemCenter: {
    textAlign: 'center',
  },
  root: {
    minHeight: 40,
  },
});

const ScoreStatistic = () => {
  const classes = useStyles();
  const scoreStatistics = useSelector(state => getScoreStatistics(state));

  return (
    <Grid container justify="center" direction="row" alignItems="center" className={classes.root}>
      <Grid item xs={4} className={classes.innerItemCenter}>
        <Typography variant="h6" color="primary" noWrap>
          {scoreStatistics[0].title}
        </Typography>
        <Typography>{scoreStatistics[0].value}</Typography>
      </Grid>
      <Grid item xs={4} className={classes.innerItemCenter}>
        <Typography variant="h6" color="primary" noWrap>
          {scoreStatistics[1].title}
        </Typography>
        <Typography>{scoreStatistics[1].value}</Typography>
      </Grid>
      <Grid item xs={4} className={classes.innerItemCenter}>
        <Typography variant="h6" color="primary" noWrap>
          {scoreStatistics[2].title}
        </Typography>
        <Typography>{scoreStatistics[2].value}</Typography>
      </Grid>
    </Grid>
  );
};

export default ScoreStatistic;

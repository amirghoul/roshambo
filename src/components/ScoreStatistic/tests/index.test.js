import React from 'react';
import { render } from '../../../utils/testing';
import Component from '..';

describe('components | ScoreStatisticApp | index', () => {
  it('should render component', () => {
    const { container } = render(<Component />);

    expect(container).toMatchSnapshot();
  });
});

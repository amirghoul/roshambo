import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import App from './components/App';
import theme from './theme';
import configureStore from './store';
import { getHuman, getComputer, getStatus, getShapes, getScoreHistory, getScoreStatistics } from './selectors/game';

const initialState = {
  human: getHuman(''),
  computer: getComputer(''),
  status: getStatus(''),
  shapes: getShapes(''),
  scoreHistory: getScoreHistory(''),
  scoreStatistics: getScoreStatistics(''),
};

const myStore = configureStore(initialState);

const ROOT = (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Provider store={myStore}>
      <App />
    </Provider>
  </ThemeProvider>
);

ReactDOM.render(ROOT, document.getElementById('root'));

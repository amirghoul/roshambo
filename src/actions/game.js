import { reduce, assoc } from 'ramda';
import {
  getComputerHandAPI,
  getWinnerAPI,
  addToHistory,
  incrementScore,
  PLAYING,
  isComputer,
  WAIT_RESULT,
} from '../game';
import { getHuman, getComputer, getShapes, getScoreStatistics, getStatus } from '../selectors/game';

export const START_GAME = 'START_GAME';
export const PLAY = 'PLAY';
export const UPDATE_SCORE = 'UPDATE_SCORE';

export const startGame = humanShape => async (dispatch, getState) => {
  const human = getHuman(getState());
  const computer = getComputer(getState());

  const promises = [
    () =>
      dispatch({
        type: START_GAME,
        status: PLAYING,
      }),
    () => dispatch(play(human, humanShape)),
    () =>
      getComputerHandAPI().then(computerShape => {
        dispatch(play(computer, computerShape));
      }),
    () => dispatch(updateScore()),
  ];

  return reduce((acc, callback) => acc.then(() => callback()), Promise.resolve(), promises);
};

export const play = (player, shape) => (dispatch, getState) => {
  const shapes = getShapes(getState());
  let status = getStatus(getState());
  const newShapes = assoc(player.name, shape, shapes);
  if (isComputer(player)) status = WAIT_RESULT;
  return dispatch({
    type: PLAY,
    status,
    shapes: newShapes,
  });
};

export const updateScore = () => (dispatch, getState) => {
  const shapes = getShapes(getState());
  const computer = getComputer(getState());
  const human = getHuman(getState());
  const scoreStatistics = getScoreStatistics(getState());

  return getWinnerAPI(shapes, computer, human).then(([winner, status]) => {
    dispatch({
      type: UPDATE_SCORE,
      status,
      scoreHistory: addToHistory(shapes, winner),
      scoreStatistics: incrementScore(winner, scoreStatistics),
    });
  });
};

import { START_GAME, PLAY, UPDATE_SCORE } from '../actions/game';

export default (state, action) => {
  switch (action.type) {
    case START_GAME:
      return {
        ...state,
        status: action.status,
      };
    case PLAY:
      return {
        ...state,
        status: action.status,
        shapes: action.shapes, // change Human shape then computer shape
      };
    case UPDATE_SCORE:
      return {
        ...state,
        status: action.status,
        scoreHistory: [...state.scoreHistory, { id: state.scoreHistory.length + 1, ...action.scoreHistory }],
        scoreStatistics: action.scoreStatistics,
      };

    default:
      return state;
  }
};

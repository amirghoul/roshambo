import { assoc, prop } from 'ramda';
import Axios from 'axios';

// -----> CONSTANTS <-----
// player
export const COMPUTER = 'computer';
export const HUMAN = 'human';

// Shapes :
export const PAPER = { name: 'paper', icon: 'fa fa-hand-paper-o' };
export const ROCK = { name: 'rock', icon: 'fa fa-hand-rock-o' };
export const SCISSORS = { name: 'scissors', icon: 'fa fa-hand-scissors-o' };
export const SHAPES = [PAPER, ROCK, SCISSORS];

// Initial Shapes
export const INITIAL_SHAPES = {
  human: { name: 'begin', icon: 'fas fa-hand-pointer' },
  computer: { name: 'begin', icon: 'fab fa-reddit-alien' },
};

// status
export const LETS_BEGIN = 'Play';
export const PLAYING = 'wait ..';
export const WAIT_RESULT = 'wait ...';

// Result :
export const WIN = 'Win';
export const LOSE = 'Lose';
export const NO_WINNER = 'Tie';

// history :
export const EMPTY_ROUNDS_HISTORY = [];
export const INITIAL_SCORE_STATISTIC = [
  { title: HUMAN, value: 0 },
  { title: NO_WINNER, value: 0 },
  { title: COMPUTER, value: 0 },
];

// -----> Game Logic <-----
// status
export const isPlaying = status => status === PLAYING;
export const isComputer = player => player.isComputer === true;
export const isWaitingResult = status => status === WAIT_RESULT;

// score & history
export const addToHistory = (lastShapes, winner) => {
  const lastRoundScore = { ...lastShapes, winner };
  return lastRoundScore;
};
export const incrementScore = (winner, lastScore) => {
  const newScore = lastScore;
  if (winner) {
    if (winner.isComputer) {
      newScore[2].value += 1;
    } else {
      newScore[0].value += 1;
    }
  } else {
    newScore[1].value += 1;
  }
  return newScore;
};

// play
export const play = (shape, shapes, player1, player2) => {
  const randomShape = SHAPES[Math.floor(Math.random() * SHAPES.length)];
  let newShapes = assoc(player1.name, shape, shapes);
  newShapes = assoc(player2.name, randomShape, newShapes);
  return newShapes;
};

// ---------> API <---------
const COMPUTER_PLAY_API = '/api/randomshape';
const GET_WINNER_API = '/api/winner';

export const getComputerHandAPI = async () => {
  const data = await Axios.get(COMPUTER_PLAY_API).then(prop('data'));

  return data;
};

export const getWinnerAPI = async (shapes, computer, human) => {
  const body = { shapes, computer, human };
  const data = await Axios.post(GET_WINNER_API, body).then(prop('data'));

  return data;
};

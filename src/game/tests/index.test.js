import { isPlaying, PLAYING } from '..';

describe('Game', () => {
  it('Should get right status *', () => {
    expect(isPlaying(PLAYING)).toBeTruthy();
    expect(isPlaying('')).toBeFalsy();
  });
});
